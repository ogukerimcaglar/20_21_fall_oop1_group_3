#pragma once
#include "RobotInterface.h"
#include "RobotControl.h"

using namespace std;

typedef class PioneerRobotInterface : public RobotInterface 
{
protected:
	PioneerRobotAPI* api;
	RobotControl* robot;
public:
	PioneerRobotInterface(PioneerRobotAPI* api, RobotControl* robot);
	void turnLeft();
	Pose getPose();
	void turnRight();
	void forward(float speed);
	void print() const;
	void stopMove();
	void backward(float speed);
	void updateSensor(float* ranges);
	void setPose(Pose position);
	void stopTurn();
}PioneerRobotInterface;