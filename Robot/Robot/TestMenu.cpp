#include "TestMenu.h"

TestMenu::TestMenu(PioneerRobotAPI* api)
{
	this->api = api;
}

void TestMenu::Show()
{
	MotionMenu* menu;
	SensorMenu* sMenu;
	char c;
	cout << "1. RobotControl Test" << endl <<
		"2. Sensor Test" << endl << "3. Pose Test" << endl << "4. Path Test" << endl <<
		"5. RobotOperator Test" << endl << "6. Record Test" << endl << "7. Back" <<
		"Choose one: ";
	cin >> c;
	if ((int)c >= 49 && (int)c <= 55)
	{
		switch (c)
		{
		case 49:
			menu = new MotionMenu(new RobotControl());
			menu->Show();
			break;
		case 50:
			sMenu = new SensorMenu(this->api);
			sMenu->Show();
			break;
		case 51:
			this->PostTest();
			break;
		case 52:
			this->PathTest();
			break;
		case 53:
			this->RobotOperatorTest();
			break;
		case 54:
			this->RecordTest();
			break;
		case 55:
			this->Back();
			break;
		}

	}
	else
		cout << "Error, check the input!";

}

void TestMenu::PostTest()
{
	Pose* p1 = new Pose(10, 10, 45);
	Pose* p2 = new Pose(5, 5, 135);

	Pose pose = (*p1) + (*p2);

	cout << "Pose p1(10, 10, 45)" << endl <<
		"Pose p2(5, 5, 135)" << endl << "Pose pose = p1 + p2 = " <<
		" (" << pose.getX() << ", " << pose.getY() << ", " << pose.getTh() << ")"
		<< endl << "Angle between p1 and p2 : " << p1->findAngleTo(*p2) << endl <<
		"Distance between p1 and p2 : " << p1->findDistanceTo(*p2) << endl;
}

void TestMenu::PathTest()
{
	Path* path = new Path();
	path->addPos(new Pose(3, 4, 37));
	path->addPos(new Pose(4, 3, 53));
	path->addPos(new Pose(5, 5, 45));
	path->print();
	delete path;
}

void TestMenu::RobotOperatorTest()
{
	int code = 1264;
	RobotOperator rbt("Kerim", "Caglar", code);
	cout << "Robot Operator Class Test :" << endl;
	rbt.print();
	cout << "Code : " << code << endl;
	cout << "Access Code : ";
	if (rbt.checkAccessCode(1264)) {
		cout << "True" << endl;
	}
	else {
		cout << "False" << endl;
	}
}

void TestMenu::Back()
{
	//MainMenu m = MainMenu(this->api);
}

void TestMenu::RecordTest()
{
	Record* rec = new Record("TestFile.txt");
	rec->writeLine("Test1");
	rec->writeLine("Test2");
	rec->writeLine("Test3");
	cout << "Writing into the file: Test1, Test2, Test3" << endl;
	
	string* str = new string[3];
	cout << "Reading the file:" << endl;
	for (int i = 0; i < 3; i++)
	{
		str[i] = rec->readLine();
		cout << str[i] << endl;
	}
	rec->closeFile();
}