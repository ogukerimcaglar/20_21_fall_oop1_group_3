#pragma once
#include <iostream>
using namespace std;

class Encryption {
public:

	static int encrypt(int code);
	static int decrypt(int code);
};