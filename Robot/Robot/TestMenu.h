#pragma once
#include <iostream>
#include "Path.h"
#include "RobotOperator.h"
#include "QuitMenu.h"
#include "MotionMenu.h"
#include "SensorMenu.h"
#include "PioneerRobotAPI.h"
#include "Record.h"

using namespace std;
typedef class TestMenu : QuitMenu
{
private:
	PioneerRobotAPI* api;
public:
	TestMenu(PioneerRobotAPI* api);
	void Show();
	void PostTest();
	void PathTest();
	void RobotOperatorTest();
	void RecordTest();
	void Back();
}TestMenu;