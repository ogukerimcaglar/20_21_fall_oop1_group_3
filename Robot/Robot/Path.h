#pragma once
#include <iostream>
#include "Node.h"
#include <fstream>
using namespace std;

typedef class Path
{
	friend ostream& operator<<(ostream& out, Path);
	friend istream& operator>>(istream& in, Path);
private:
	Node* tail;
	Node* head;
	int number;
public:
	Path() {}
	bool addPos(Pose* pose);
	void print();
	Node operator[](int i);
	Pose getPos(int index);
	bool removePos(int index);
	bool insertPos(int index, Pose pose);
	int getNumber() const;
}Path;
