#include "RobotOperator.h"

int RobotOperator::encryptCode(int code) {
	return Encryption::encrypt(code);
}
int RobotOperator::decryptCode(int code) {
	return Encryption::decrypt(code);
}
RobotOperator::RobotOperator(string firstName, string lastName, int accessCode) {
	this->name = firstName;
	this->surname = lastName;
	this->accessCode = encryptCode(accessCode);
	this->accessState = true;
}
bool RobotOperator::checkAccessCode(int code) {
	if (code == this->accessCode) {
		return true;
	} 
	else {
		return false;	
	} 
}
void RobotOperator::print() {
	cout << "First Name : " + this->name + "\nLast Name : " + this->surname + "\n";
	cout << "Access State is : ";
	if (this->accessState) {
		cout << "True" << endl;	
	} 
	else {
		cout << "False" << endl;
	}
}
