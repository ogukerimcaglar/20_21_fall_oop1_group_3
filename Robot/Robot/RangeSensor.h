#pragma once
#include <iostream>
#include <stdlib.h>
#include "PioneerRobotAPI.h"
using namespace std;

typedef class RangeSensor 
{
protected:
	float* ranges;
	PioneerRobotAPI* robotAPI;
public:
	RangeSensor();
	virtual float getMin(int& index);
	virtual float getMax(int& index);
	virtual float getRange(int index) const;
	virtual float getAngle(int index) const;
	virtual void updateSensor(float* ranges) = 0;
	virtual float getClosestRange(float startAngle, float endAngle, float& angle);
	float operator[](int i);
}RangeSensor;