#pragma once
#include "PioneerRobotAPI.h"
#include "QuitMenu.h"
#include <iostream>

using namespace std;

typedef class ConnectionMenu : QuitMenu
{
private:
	PioneerRobotAPI* api;
public:
	ConnectionMenu(PioneerRobotAPI* api);
	void Show();
	void Connect();
	void Disconnect();
	void Back();
}ConnectionMenu;