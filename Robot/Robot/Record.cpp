#include "Record.h"
#include <fstream>
#include <iostream>
#include <string>
using namespace std;

Record::Record(string fileName)
{
	this->fileName = fileName;
}

bool Record::openFile() {
	this->file.open(this->fileName, ios::in);

	return this->file.is_open();
}

bool Record::closeFile() {
	this->file.close();

	return !(this->file.is_open());
}
/***
	Set file name
***/
void Record::setFileName(string name) {
	this->fileName = name;
}

/***
	Read file line by line
***/
string Record::readLine() {
	string line;
	if (this->file.is_open())
		getline(this->file, line);
	else
		cout << "File is not found!" << endl;
	return line;
}
/***
	Write file
***/
bool Record::writeLine(string data) {
	ofstream stream = ofstream(this->fileName, ios::app);

	if (stream.is_open())
		stream << data;
	else
		cout << "Could not write data to file.";

	stream.close();

	return stream.is_open();
}
bool Record::operator <<(string str) {
	return writeLine(str);
}
bool Record::operator >>(string &str) {
	if ((str = this->readLine()) != "\0")
		return true;
	else
		return false;
}
