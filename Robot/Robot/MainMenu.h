#pragma once
#include "PioneerRobotAPI.h"
#include "ConnectionMenu.h"
#include "MotionMenu.h"
#include "SensorMenu.h"
#include "TestMenu.h"

typedef class MainMenu : QuitMenu
{
private:
	PioneerRobotAPI* api;
	ConnectionMenu* connection;
	MotionMenu* motion;
	SensorMenu* sensor;
	TestMenu* test;
public:
	MainMenu(PioneerRobotAPI* api);
	void Connection();
	void Motion();
	void Sensor();
	void Test();

}MainMenu;