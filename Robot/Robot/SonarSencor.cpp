#include "SonarSensor.h"


float SonarSensor::getRange(int index) const
{
	return this->ranges[index];
}

float SonarSensor::getAngle(int index) const
{
	float angle = 0;
	switch (index)
	{
	case 0:
		angle = 90;
		break;
	case 1:
		angle = 50;
		break;
	case 2:
		angle = 30;
		break;
	case 3:
		angle = 10;
		break;
	case 4:
		angle = -10;
		break;
	case 5:
		angle = -30;
		break;
	case 6:
		angle = -50;
		break;
	case 7:
		angle = -90;
		break;
	case 8:
		angle = -90;
		break;
	case 9:
		angle = -130;
		break;
	case 10:
		angle = -150;
		break;
	case 11:
		angle = -170;
		break;
	case 12:
		angle = 170;
		break;
	case 13:
		angle = 150;
		break;
	case 14:
		angle = 130;
		break;
	case 15:
		angle = 90;
		break;
	default:
		break;
	}
	angle += this->robotAPI->getTh();

	if (angle > 180) angle = angle - 180;
	else if (angle < -180) angle = angle + 180;
		
	return angle;
}

void SonarSensor::updateSensor(float ranges[])
{
	int i = 0;
	while (!ranges[i])
	{
		this->ranges[i] = ranges[i];
		i++;
	}
}

float SonarSensor::getMin(int &index)
{
	float min = this->ranges[0];
	int temp;
	for (int i = 0; i < 16; i++)
	{
		if (min > this->ranges[i])
		{
			min = this->ranges[i];
			temp = i;
		}
	}

	index = temp;
	return min;
}

float SonarSensor::getMax(int &index)
{
	float max = this->ranges[0];
	int temp;
	for (int i = 0; i < 16; i++)
	{
		if (max < this->ranges[i])
		{
			max = this->ranges[i];
			temp = i;
		}
	}

	index = temp;
	return max;
}

float SonarSensor::operator[](int i) {
	return this->ranges[i];
}
