#include "PioneerRobotInterface.h"

PioneerRobotInterface::PioneerRobotInterface(PioneerRobotAPI* api, RobotControl* robot)
{
	this->api = api;
	this->robot = robot;
}

void PioneerRobotInterface::turnLeft() 
{
	this->robot->turnLeft();
}
void PioneerRobotInterface::turnRight() 
{
	this->robot->turnRight();
}
void PioneerRobotInterface::forward(float speed) 
{
	this->robot->forward(speed);
}

void PioneerRobotInterface::setPose(Pose position)
{
	this->robot->setPose(position);
}
void PioneerRobotInterface::stopTurn()
{
	this->robot->stopTurn();
}
void PioneerRobotInterface::stopMove() 
{
	this->robot->stopMove();
}
void PioneerRobotInterface::updateSensor(float* ranges) 
{
	this->robot->updateSensor(ranges);
}
void PioneerRobotInterface::print() const 
{
	this->robot->print();
}
void PioneerRobotInterface::backward(float speed) 
{
	this->robot->backward(speed);
}
Pose PioneerRobotInterface::getPose() 
{
	return this->robot->getPose();
}