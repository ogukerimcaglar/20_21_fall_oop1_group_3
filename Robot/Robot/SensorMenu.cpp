#include "SensorMenu.h"
#include "MainMenu.h"

SensorMenu::SensorMenu(PioneerRobotAPI* api)
{
	this->api = api;
	sonar = new SonarSensor();
}
void SensorMenu::Show()
{
	char c;
	cout << "Sensor Menu" << endl <<
		"1. Get Range" << endl << "2. Get Max" <<
		endl << "3. Get Min" << endl << "4. Get Angle" << endl
		<< "5. Back" << endl << "6. Quit" << endl << "Choose one: ";
	cin >> c;

	if ((int)c >= 49 && (int)c <= 54)
	{
		switch (c)
		{
		case 49:
			this->GetRange();
			break;
		case 50:
			this->GetMax();
			break;
		case 51:
			this->GetMin();
			break;
		case 52:
			this->GetAngle();
			break;
		case 53:
			this->Back();
			break;
		case 54:
			this->Quit();
			break;
		default:
			break;
		}
	}
	else
		cout << "Error, check the input!";

	this->Back();
}
void SensorMenu::GetRange()
{
	int i;
	cout << "Enter an index : ";
	cin >> i;
	cout << "Range at " << i << "th index : " << sonar->getRange(i) << endl;
}
void SensorMenu::GetMin()
{
	int i = 0;
	cout << "Min Range :" << sonar->getMin(i) << "\t";
	cout << "Index is : " << i << endl;
}
void SensorMenu::GetMax()
{
	int i = 0;
	cout << "Max Range :" << sonar->getMax(i) << "\t";
	cout << "Index is : " << i << endl;
}
void SensorMenu::GetAngle()
{
	int i;
	cout << "Enter an index : ";
	cin >> i;
	cout << "Angle at " << i << "th index : " << sonar->getAngle(i) << endl;
}
void SensorMenu::Back()
{
	MainMenu menu(this->api);
}