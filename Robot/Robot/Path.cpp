#include "Path.h"

bool Path::addPos(Pose* pose) {
	Node* n = new Node();
	n->pose = *pose;

	if (n == NULL)
		return false;
	if (this->head == NULL) {
		this->head = n;
		this->tail = n;
	}
	else {
		this->tail->next = n;
		this->tail = n;
	}
	this->number++;
	return true;
}

void Path::print() {
	float x1, y1, th1, x2, y2, th2;
	this->head->pose.getPose(x1, y1, th1);
	this->tail->pose.getPose(x2, y2, th2);

	cout << "Head Position Values (x1, y1, th1): " << x1 << " " << y1 << " " << th1 << endl;
	cout << "Tail Position Values (x2, y2, th2): " << x2 << " " << y2 << " " << th2 << endl;
	
}

Node Path::operator[](int i) {

	Node* p = this->head;

	for (int j = 0; j < this->number; j++) 
	{
		if (i == j) {
			break;
		}
		else {
			p = p->next;
		}
	}
	return *p;
}

Pose Path::getPos(int index) {	
	return this[index].getPos(index);
}

bool Path::removePos(int index) { 
	Node* temp = head;
	if (index == 0) 
	{
		head = temp->next;
		free(temp);
		return true;
	}
	for (int i = 0; i < index-1; i++)
		temp = temp->next;

	Node* temp2 = temp->next;
	temp->next = temp2->next;
	free(temp2);
	
	this->number--;

	return false;
}

bool Path::insertPos(int index, Pose pose) {
	if (this->head == NULL)
		return false;

	Node* temp = new Node();
	temp->pose = pose;
	temp->next = NULL;

	if (index == 0){
		temp->next = head;
		head = temp;
		return true;
	}

	Node* temp2 = head;
	for (int i = 0; i < index-1; i++){
		temp2 = temp2->next;
	}

	temp->next = temp2->next;
	temp2->next = temp;
	delete temp;

	this->number++;

	return true;	
}

ostream& operator<<(ostream& out, Path path) {
	float x1, y1, th1, x2, y2, th2;

	path.head->pose.getPose(x1, y1, th1);
	path.tail->pose.getPose(x2, y2, th2);

	out << "Head Position Values (x1, y1, th1): " << x1 << " " << y1 << " " << th1 << endl
	 << "Tail Position Values (x2, y2, th2): " << x2 << " " << y2 << " " << th2 << endl;

	return out;
}

istream& operator>>(istream& in, Path path) {

	Pose* pose = new Pose();
	float x, y, th;
	in >> x >> y >> th;
	pose->setPose(x, y, th);
	path.addPos(pose);

	return in;
}

int Path::getNumber() const
{
	return this->number;
}
