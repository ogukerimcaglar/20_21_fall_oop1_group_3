#include "ConnectionMenu.h"
#include "QuitMenu.h"
#include "MainMenu.h"

ConnectionMenu::ConnectionMenu(PioneerRobotAPI* api)
{
	this->api = api;
}

void ConnectionMenu::Show() {
	char c;
	cout << "Connection Menu" << endl <<
		"1. Connect" << endl << "2. Disconnect" <<
		endl << "3. Back" << endl << "4. Quit" << endl
		<< "Choose one: ";
	cin >> c;

	if ((int)c >= 49 && (int)c <= 52)
	{
		switch (c)
		{
		case 49:
			this->Connect();
			break;
		case 50:
			this->Disconnect();
			break;
		case 51:
			this->Back();
			break;
		case 52:
			this->Quit();
			break;
		default:
			break;
		}
	}
	else
		cout << "Error, check the input!";

	this->Back();
}

void ConnectionMenu::Connect()
{
	bool state = this->api->connect();
	if (state)
		cout << "Robot is connected!";
	else
		cout << "Robot cannot be connected!";

	cout << endl;
}

void ConnectionMenu::Disconnect()
{
	bool state = this->api->disconnect();
	if (state)
		cout << "Robot is disconnected!";
	else
		cout << "Robot cannot be disconnected!";
	cout << endl;
}

void ConnectionMenu::Back()
{
	MainMenu menu(this->api);
}