#include "MainMenu.h"

using namespace std;

PioneerRobotAPI *robot;
float sonars[16];
float laserData[181];

void print() {
	cout << "MyPose is (" << robot->getX() << "," << robot->getY() << "," << robot->getTh() << ")" << endl;
	cout << "Sonar ranges are [ ";
	robot->getSonarRange(sonars);
	for (int i = 0; i < 16; i++) {
		cout << sonars[i] << " ";
	}
	cout << "]" << endl;
	cout << "Laser ranges are [ ";
	robot->getLaserRange(laserData);
	for (int i = 0; i < 181; i++) {
		cout << laserData[i] << " ";
	}
	cout << "]" << endl;
}

int main() {

	robot=new PioneerRobotAPI;
	
	MainMenu m = MainMenu(robot);

	robot->disconnect();
	delete robot;
	return 0;
	
}