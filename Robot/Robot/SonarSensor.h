#pragma once
#include "PioneerRobotAPI.h"
#include "RangeSensor.h"

typedef class SonarSensor : public RangeSensor
{
public:
	SonarSensor() {}
	float getRange(int index) const;
	void updateSensor(float ranges[]);
	float getMax(int &index);
	float getMin(int &index);
	float operator[](int i);
	float getAngle(int index) const;
}SonarSensor;
