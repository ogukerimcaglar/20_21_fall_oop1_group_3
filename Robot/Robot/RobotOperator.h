#pragma once
#include <iostream>
#include <string>
#include "Encryption.h"

using namespace std;

class RobotOperator {
	string name;
	string surname;
	unsigned int accessCode;
	bool accessState;
	int encryptCode(int code);
	int decryptCode(int code);
public :
	RobotOperator(string _name, string _surname, int _accessCode);
	bool checkAccessCode(int code);
	void print();
};
