#pragma once
#include "Pose.h"
#include "PioneerRobotAPI.h"
#include "RobotInterface.h"
#include "Path.h"
#include "Record.h"
using namespace std;

typedef class RobotControl : public RobotInterface
{
private:
	RobotOperator* op;
	bool auth;
public:
	RobotControl();
	void turnLeft();
	void turnRight();
	void forward(float speed);
	void print() const;
	void backward(float speed);
	Pose getPose();
	void setPose(Pose position);
	void stopTurn();
	void stopMove();
	bool addToPath();
	bool clearPath();
	bool openAccess(int);
	bool closeAccess(int);
	bool recordPathToFile();
}RobotControl;