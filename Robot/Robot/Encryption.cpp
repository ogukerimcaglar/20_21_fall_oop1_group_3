#include "Encryption.h"

int Encryption::encrypt(int code) {
	int hash = code;
	int encrypted = 0;
	if (hash >= 10000) return -1;

	int digits[4];
	for (int i = 0; i < 4; i++) {

		digits[i] = hash % 10;
		hash = hash / 10;

		digits[i] = (digits[i] + 7) % 10;

		encrypted += digits[i] * pow(10, i);
	}
	return encrypted;
}
int Encryption::decrypt(int code) {
	int hash = code;
	int decrypted = 0;
	if (hash >= 10000) return -1;

	int digits[4];
	for (int i = 0; i < 4; i++) {

		digits[i] = hash % 10;
		hash = hash / 10;

		digits[i] = (digits[i] + 10 - 7) % 10;

		decrypted += digits[i] * pow(10, i);
	}
	return decrypted;
}
