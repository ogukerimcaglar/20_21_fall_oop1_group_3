#pragma once
#include "PioneerRobotAPI.h"
#include "QuitMenu.h"
#include <iostream>
#include "RobotControl.h"

using namespace std;

typedef class MotionMenu : QuitMenu
{
private:
	RobotControl* robot;
public:
	MotionMenu(RobotControl* robot);
	void Show();
	void MoveRobot();
	void TurnLeft();
	void TurnRight();
	void Forward();
	void CloseWall();
	void AddToPath();
	void ClearPath();
	void RecordPath();
	void OpenAccess();
	void CloseAccess();
}MotionMenu;