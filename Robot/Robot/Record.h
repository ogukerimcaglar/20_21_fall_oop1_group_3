#pragma once
#include <iostream>
#include <fstream>
using namespace std;


typedef class Record
{
private:
	string fileName;
	fstream file;
public:
	Record(string fileName);
	bool openFile();
	bool closeFile();
	void setFileName(string name);
	string readLine();
	bool writeLine(string str);
	bool operator <<(string str);
	bool operator >>(string& str);
}Record;

