#include "LaserSensor.h"

float LaserSensor::getRange(int index) const
{
	return this->ranges[index];
}

float LaserSensor::getAngle(int index) const
{
	// index == 0 angle= -90;
	// index == 90 angle = 0;
	// index == 180 angle = 90;
	return (index - 90);
}

void LaserSensor::updateSensor(float ranges[])
{
	int i = 0;
	while (!ranges[i])
	{
		this->ranges[i] = ranges[i];
		i++;
	}
}

float LaserSensor::getMin(int &index)
{
	float min = this->ranges[0];
	int ind;
	for (int i = 0; i < 181; i++)
	{
		if (min > this->ranges[i])
		{
			min = this->ranges[i];
			ind = i;
		}
	}

	index = ind;
	return min;
}

float LaserSensor::getMax(int &index)
{
	float max = this->ranges[0];
	int ind;
	for (int i = 0; i < 181; i++)
	{
		if (max < this->ranges[i])
		{
			max = this->ranges[i];
			ind = i;
		}
	}

	index = ind;
	return max;
}

float LaserSensor::getClosestRange(float startAngle, float endAngle, float &angle)
{
	int startIndex = (startAngle) + 90;
	int endIndex = (endAngle)+90;

	float minRange = this->ranges[startIndex];
	for (int i = startIndex; i <= endIndex; i++)
	{
		if (minRange > this->ranges[i])
			minRange = this->ranges[i];
	}

	angle = minRange - 90;
	return minRange;
}