#pragma once
#include "PioneerRobotAPI.h"
#include "RangeSensor.h"

typedef class LaserSensor : public RangeSensor
{
public:
	LaserSensor() {}
	float getRange(int index) const;
	void updateSensor(float ranges[]);
	float getMax(int &index);
	float getMin(int &index);
	float getAngle(int index) const;
	float getClosestRange(float startAngle, float endAngle, float &angle);
}LaserSensor;