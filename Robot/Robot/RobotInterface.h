#pragma once
#include "Pose.h"
#include "PioneerRobotAPI.h"
#include "RobotOperator.h"
#include "SonarSensor.h"

using namespace std;

typedef class RobotInterface 
{
protected:
	PioneerRobotAPI* robotAPI;
	Pose* position;
	int state;
public:
	RobotInterface();
	virtual void turnLeft() = 0;
	virtual void forward(float speed) = 0;
	virtual void print() const = 0;
	virtual void backward(float speed) = 0;
	virtual void turnRight() = 0;
	virtual void setPose(Pose position) = 0;
	virtual void stopTurn() = 0;
	virtual Pose getPose() = 0;
	virtual void stopMove() = 0;
	void updateSensor(float* ranges);
}RobotInterface;