#pragma once

typedef struct Pose
{
private:
	float x;
	float y;
	float th;
public:
	Pose();
	Pose(float x, float y, float th);
	float getX() const;
	float getY() const;
	float getTh() const;
	void setX(float x);
	void setY(float y);
	void setTh(float th);
	bool operator ==(const Pose& other);
	Pose operator +(const Pose& other);
	Pose operator -(const Pose& other);
	Pose& operator +=(const Pose& other);
	Pose& operator -=(const Pose& other);
	bool operator <(const Pose& other);
	void getPose(float &x, float &y, float &th) const;
	void setPose(float x, float y, float th);
	float findDistanceTo(Pose pos);
	float findAngleTo(Pose pos);
}Pose;