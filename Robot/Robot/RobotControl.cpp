#include "RobotControl.h"

RobotControl::RobotControl()
{
	this->position = new Pose();
	this->robotAPI = new PioneerRobotAPI();
	this->state = 0;
	string name, surname;
	int auth;
	cout << "Enter the name: ";
	cin >> name;
	cout << "Enter the surname: ";
	cin >> surname;
	cout << "Enter the access Code (4-digits): ";
	cin >> auth;
	this->op = new RobotOperator(name, surname, auth);
	this->auth = false;
}
void RobotControl::turnLeft()
{
	this->state = PioneerRobotAPI::DIRECTION::left;
	this->robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::left);
}

void RobotControl::turnRight()
{
	this->state = PioneerRobotAPI::DIRECTION::right;
	this->robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::right);
}

void RobotControl::forward(float speed)
{
	this->robotAPI->moveRobot(speed);
}

void RobotControl::backward(float speed)
{
	if (speed > 0) 
		speed *= -1;
	this->robotAPI->moveRobot(speed);
}

Pose RobotControl::getPose()
{
	Pose pos(this->position->getX(), this->position->getY(), this->position->getTh());
	return pos;
}

void RobotControl::setPose(Pose position)
{
	this->position->setPose(position.getX(), position.getY(), position.getTh());
}

void RobotControl::stopTurn()
{
	if (this->state == PioneerRobotAPI::DIRECTION::right) {
		this->turnLeft();
	}
	else if (this->state == PioneerRobotAPI::DIRECTION::left) {
		this->turnRight();
	}
	else {
		//forward is forward;
	}
}

void RobotControl::print() const
{
	cout << "Position (x, y): " << this->position->getX() << ", " << this->position->getY() << endl;
	cout << "Angle : " << this->position->getTh() << endl;
	cout << "Direction : " << this->state << endl;
}

void RobotControl::stopMove()
{
	this->robotAPI->stopRobot();
}

bool RobotControl::addToPath() {
	Path* path = new Path();
	Pose pos(this->position->getX(), this->position->getY(), this->position->getTh());

	return path->addPos(&pos);
}
bool RobotControl::clearPath() {
	Path* path = new Path();
	int numb = path->getNumber();

	for (int i = 0; i < numb; i++) {
		path->removePos(i);
	}

	if (path->getNumber() == 0)
		return true;
	else
		return false;
}
bool RobotControl::recordPathToFile() {
	Path* path = new Path();
	Record* record = new Record("log.txt");
	int numb = path->getNumber();
	Pose pos;
	bool hasWritten = false;
	for (int i = 0; i < numb; i++) {
		pos = path->getPos(i);
		hasWritten = record->writeLine("X: " + to_string(pos.getX()) + "Y: " + to_string(pos.getY()) + "th: " + to_string(pos.getTh()));
	}
	return hasWritten;
}
bool RobotControl::openAccess(int code)
{
	bool state = this->op->checkAccessCode(code);
	this->auth = state;
	return state;
}
bool RobotControl::closeAccess(int code)
{
	bool state = this->op->checkAccessCode(code);
	this->auth = !state;
	return state;
}