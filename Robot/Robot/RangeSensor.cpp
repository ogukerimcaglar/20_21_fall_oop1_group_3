#include "RangeSensor.h"

using namespace std;

RangeSensor::RangeSensor()
{
	this->ranges = NULL;
	this->robotAPI = nullptr;
}

float RangeSensor::operator[](int i) {
	return this->ranges[i];
}