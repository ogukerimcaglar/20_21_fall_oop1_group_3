#pragma once
#include "PioneerRobotAPI.h"
#include "QuitMenu.h"
#include "SonarSensor.h"
#include <iostream>

using namespace std;

typedef class SensorMenu : QuitMenu
{
private:
	SonarSensor* sonar;
	PioneerRobotAPI* api;
public:
	SensorMenu(PioneerRobotAPI* api);
	void Show();
	void GetRange();
	void GetMin();
	void GetMax();
	void GetAngle();
	void Back();
}SensorMenu;