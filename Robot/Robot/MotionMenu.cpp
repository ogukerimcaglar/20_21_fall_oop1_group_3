#include "MotionMenu.h"
#include "SonarSensor.h"
#include "QuitMenu.h"
#include "MainMenu.h"

MotionMenu::MotionMenu(RobotControl* robot)
{
	this->robot = robot;
}

void MotionMenu::Show() {
	char c;
	cout << "Motion Menu" << endl <<
		"1. Move Robot" << endl << "2. Turn Left" << endl << "3. Turn Right" << endl
		<< "4. Forward" << endl
		<< "5. Close Wall" << endl << "6. Open Access" << endl << "7. Close Access" << endl
		<< "8. Record Path" << endl << "9. Add to Path" << endl << "0. Clear Path" << endl
		<< "Choose one: ";
	cin >> c;

	if ((int)c >= 48 && (int)c <= 57)
	{
		switch (c)
		{
		case 49:
			this->MoveRobot();
			break;
		case 50:
			this->TurnLeft();
			break;
		case 51:
			this->TurnRight();
			break;
		case 52:
			this->Forward();
			break;
		case 53:
			this->CloseWall();
			break;
		case 54:
			this->OpenAccess();
			break;
		case 55:
			this->CloseAccess();
			break;
		case 56:
			this->RecordPath();
			break;
		case 57:
			this->AddToPath();
			break;
		case 48:
			this->ClearPath();
			break;
		default:
			break;
		}
	}
	else
		cout << "Error, check the input!";
}

void MotionMenu::MoveRobot()
{
	float speed = 10;
	cout << "Enter the speed of the robot: ";
	cin >> speed;
	this->robot->forward(speed);

	cout << "Robot is moving with " << speed << "milimeter/sec" << endl;
}

void MotionMenu::TurnLeft()
{
	this->robot->turnLeft();
}

void MotionMenu::TurnRight()
{
	this->robot->turnRight();
}

void MotionMenu::Forward()
{
	this->robot->forward(100);
}

void MotionMenu::CloseWall()
{
	SonarSensor* sensor = new SonarSensor();
	int index = 0;
	cout << "Closest Wall Range: " << sensor->getMin(index);
	cout << "\tIndex is : " << index << endl;
	delete sensor;
}

void MotionMenu::AddToPath()
{
	cout << "State: [" << this->robot->addToPath() << "]" << endl;
}

void MotionMenu::ClearPath()
{
	cout << "State: [" << this->robot->clearPath() << "]" << endl;
}

void MotionMenu::RecordPath()
{
	cout << "State: [" << this->robot->recordPathToFile() << "]" << endl;
}

void MotionMenu::OpenAccess()
{
	int key;
	cout << "Enter key: ";
	cin >> key;
	cout << "Open access state: [" << this->robot->openAccess(key) << "]" << endl;
}

void MotionMenu::CloseAccess()
{
	int key;
	cout << "Enter key: ";
	cin >> key;
	cout << "Close access state: [" << this->robot->openAccess(key) << "]" << endl;
}