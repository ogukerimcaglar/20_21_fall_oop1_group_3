#pragma once
#include "Pose.h"

typedef struct Node
{
	Node* next;
	Pose pose;
}Node;