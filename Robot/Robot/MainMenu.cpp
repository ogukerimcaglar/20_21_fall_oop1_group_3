#include "MainMenu.h"

MainMenu::MainMenu(PioneerRobotAPI* api)
{
	this->api = api;

	this->connection = new ConnectionMenu(this->api);
	this->motion = new MotionMenu(new RobotControl());
	this->sensor = new SensorMenu(this->api);
	this->test = new TestMenu(this->api);

	char c;
	cout << "Main Menu" << endl <<
		"1. Connection" << endl << "2. Motion" <<
		endl << "3. Sensor" << endl << "4. Test" << endl
		<< "5. Quit" << endl << "Choose one: ";
	cin >> c;
	
	if ((int)c >= 49 && (int)c <= 53)
	{
		switch (c)
		{
		case 49:
			this->Connection();
			break;
		case 50:
			this->Motion();
			break;
		case 51:
			this->Sensor();
			break;
		case 52:
			this->Test();
			break;
		case 53:
			this->Quit();
			break;
		}
	}
	else
		cout << "Error, check the input!";
}

void MainMenu::Connection()
{
	this->connection->Show();
}

void MainMenu::Motion()
{
	this->motion->Show();
}

void MainMenu::Sensor()
{
	this->sensor->Show();
}

void MainMenu::Test()
{
	this->test->Show();
}