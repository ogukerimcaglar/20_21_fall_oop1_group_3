#include "Pose.h"
#include <math.h>

Pose::Pose()
{
	this->x=0;
	this->y = 0;
	this->th = 0;
}

Pose::Pose(float x, float y, float th)
{
	this->x = x;
	this->y = y;
	this->th = th;
}

float Pose::getX() const
{
	return this->x;
}

float Pose::getTh() const
{
	return this->th;
}

float Pose::getY() const
{
	return this->y;
}

void Pose::setX(float x)
{
	this->x = x;
}

void Pose::setY(float y)
{
	this->y = y;
}

void Pose::setTh(float th)
{
	this->th = th;
}

bool Pose::operator ==(const Pose& other)
{
	if (other.getX() == this->getX() && other.getY() == this->getY() && other.getTh() == this->getTh())
		return 1;
	else
		return 0;
}

Pose Pose::operator +(const Pose& other)
{
	float angle, x, y;
	angle = this->findAngleTo(other);
	x = this->x + other.getX();
	y = this->y + other.getY();
	this->setPose(x, y, angle);

	return *this;
}

Pose& Pose::operator +=(const Pose& other)
{
	float angle, x, y;
	angle = this->findAngleTo(other);
	x = this->x + other.getX();
	y = this->y + other.getY();
	this->setPose(x, y, angle);

	return *this;
}

Pose Pose::operator -(const Pose& other)
{
	float angle, x, y;
	angle = this->findAngleTo(other);
	x = this->x - other.getX();
	y = this->y - other.getY();
	this->setPose(x, y, angle);

	return *this;
}

Pose& Pose::operator -=(const Pose& other)
{
	float angle, x, y;
	angle = this->findAngleTo(other);
	x = this->x - other.getX();
	y = this->y - other.getY();
	this->setPose(x, y, angle);

	return *this;
}

bool Pose::operator <(const Pose& other)
{
	if (sqrt(pow(this->x, 2) + pow(this->y, 2)) < sqrt(pow(other.getX(), 2) + pow(other.getY(), 2)))
		return 1;
	else
		return 0;
}

void Pose::getPose(float &x, float &y, float &th) const
{
	x = this->x;
	y = this->y;
	th = this->th;
}

void Pose::setPose(float x, float y, float th)
{
	this->x = x;
	this->y = y;
	this->th = th;
}

float Pose::findDistanceTo(Pose pose)
{
	float dis;

	dis = sqrt(pow(this->x - pose.getX(), 2) + pow(this->y - pose.getY(), 2));
	return dis;
}

float Pose::findAngleTo(Pose pose)
{
	float disX, disY, angle;

	disX = this->x - pose.getX();
	disY = this->y - pose.getY();
	angle = atan(disY / disX) * 180 / 3.14;
	return angle;
}